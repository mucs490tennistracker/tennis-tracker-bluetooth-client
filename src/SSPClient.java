/**
 * A simple SSP client that connects to an SSP Server
 */

import org.json.JSONObject;

import javax.bluetooth.*;
import javax.microedition.io.*;
import java.awt.event.InputMethodListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class SSPClient implements DiscoveryListener{

    //object used for waiting
    private static Object lock=new Object();

    //vector containing the devices discovered
    private static Vector vecDevices=new Vector();

    private static String connectionURL=null;

    public static void main(String[] args) {

        try {

            System.out.println("Welcome to the TennisTracker BlueTooth Test Client");

            SSPClient client = new SSPClient();

            //display local device address and name
            LocalDevice localDevice = LocalDevice.getLocalDevice();
            System.out.println("Address: " + localDevice.getBluetoothAddress());
            System.out.println("Name: " + localDevice.getFriendlyName());

            //find devices
            DiscoveryAgent agent = localDevice.getDiscoveryAgent();

            System.out.println("Starting device inquiry...");
            agent.startInquiry(DiscoveryAgent.GIAC, client);

            try {
                synchronized (lock) {
                    lock.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            System.out.println("Device Inquiry Completed. ");

            //print all devices in vecDevices
            int deviceCount = vecDevices.size();

            if (deviceCount <= 0) {
                System.out.println("No Devices Found .");
                System.exit(0);
            } else {
                //print bluetooth device addresses and names in the format [ No. address (name) ]
                System.out.println("Bluetooth Devices: ");
                for (int i = 0; i < deviceCount; i++) {
                    RemoteDevice remoteDevice = (RemoteDevice) vecDevices.elementAt(i);
                    System.out.println((i + 1) + ". " + remoteDevice.getBluetoothAddress() + " (" + remoteDevice.getFriendlyName(true) + ")");
                }
            }

            System.out.print("Choose Device index: ");  System.out.flush();
            BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in));

            String chosenIndex = bReader.readLine();
            int index = Integer.parseInt(chosenIndex.trim());

            //check for ssp service
            RemoteDevice remoteDevice = (RemoteDevice) vecDevices.elementAt(index - 1);
            UUID[] uuidSet = new UUID[1];
            uuidSet[0] = new UUID("446118f08b1e11e29e960800200c9a66", false);

            System.out.println("\nAttempting connection to SSP Server...");
            agent.searchServices(null, uuidSet, remoteDevice, client);

            try {
                synchronized (lock) {
                    lock.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (connectionURL == null) {
                System.out.println("Device does not support Simple SSP Service.");
                System.exit(0);
            }

            //connect to the server
            StreamConnection streamConnection = (StreamConnection) Connector.open(connectionURL);

            System.out.println("Connection to SSP Server was successful!");
            OutputStream outStream = streamConnection.openOutputStream();
            InputStream inStream = streamConnection.openInputStream();

            // start threads listening for user input and for server response
            TUI ui = new TUI(outStream);
            Thread uiThread = new Thread(ui);
            MessageListener listener = new MessageListener(inStream);
            Thread listenThread = new Thread(listener);
            uiThread.start();
            listenThread.start();

            try{
                uiThread.join();
                streamConnection.close();
                outStream.close();
                inStream.close();
                listener.kill();
                listenThread.join();

            } catch(InterruptedException ex){
                ex.printStackTrace();
            }

            System.out.println("Goodbye! :]");


        } catch(IOException ex){
            System.out.println("Uh oh!  Something went wrong.");
            System.out.println(ex.getMessage());
        }

    } //main

    //methods of DiscoveryListener
    public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
        //add the device to the vector
        if(!vecDevices.contains(btDevice)){
            vecDevices.addElement(btDevice);
        }
    }

    //implement this method since services are not being discovered
    public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
        if(servRecord!=null && servRecord.length>0){
            connectionURL=servRecord[0].getConnectionURL(0,false);
        }
        synchronized(lock){
            lock.notify();
        }
    }

    //implement this method since services are not being discovered
    public void serviceSearchCompleted(int transID, int respCode) {
        synchronized(lock){
            lock.notify();
        }
    }


    public void inquiryCompleted(int discType) {
        synchronized(lock){
            lock.notify();
        }

    }//end method

}

class TUI implements Runnable{
    // codes understood by the tennis tracker API - 1 = start, 2 = stop, 3 = request data, 4 = clear data
    private final int[] commandCodes = {1, 2, 3, 4};
    private boolean waitAfterCommand;
    private boolean done;
    private Scanner textInputScanner;
    private PrintWriter writer;

    public TUI(OutputStream outputStream){
        this.done = false;
        this.waitAfterCommand = true;
        textInputScanner = new Scanner(System.in);
        this.writer = new PrintWriter(new OutputStreamWriter(outputStream));
    }

    public void run(){
        while(!done){
            System.out.print("\n1. Enter a command \n2. Toggle waiting \n3. Quit\nPlease choose an option: ");  System.out.flush();
            int input = -1;
            try{
                input = Integer.parseInt(textInputScanner.nextLine());
            } catch(NumberFormatException ex){} // handled by default case below
            switch(input){
                case 1:
                    System.out.println("\n" +
                            "1. Start Capture\n" +
                            "2. End Capture\n" +
                            "3. Get Data Captured\n" +
                            "4. Clear Data Captured\n" +
                            "Choose a command to send:");
                    System.out.flush();
                    int commandChoice = -1;
                    try{
                        commandChoice = Integer.parseInt(textInputScanner.nextLine()) - 1;
                    } catch(NumberFormatException ex){} // handled by else clause below
                    if(commandChoice < 4 && commandChoice >= 0){
                        String jsonCommandString = new JSONObject().put("command", this.commandCodes[commandChoice]).toString();
                        writer.write(jsonCommandString + "\r\n"); // command plus newline
                        writer.flush();
                        // wait for 10 seconds for response if waiting is turned on
                        if(this.waitAfterCommand){
                            try{
                                System.out.println("Waiting on response...");
                                Thread.sleep(5000);
                            } catch(InterruptedException ex){
                            }
                        }

                    } else{
                        System.out.println("Invalid choice");
                    }
                    break;
                case 2:
                    this.waitAfterCommand = !this.waitAfterCommand;
                    System.out.println("Waiting after command: " + ( this.waitAfterCommand ? "ON" : "OFF"));
                    break;
                case 3:
                    this.done = true;
                    break;
                default:
                    System.out.println("Please choose a valid option");
            }

        }

        System.out.println("Quitting interface thread...");
    }
}

/**
 * Continuously looks for input from the given stream and prints it to std out
 */
class MessageListener implements Runnable{
    public boolean keepRunning = true;
    private Scanner scanner;
//    public ArrayList<String> lines;

    public MessageListener(InputStream inStream){
        this.scanner = new Scanner(inStream);
//        this.lines = new ArrayList<String>();
    }

    public void run(){
        while(this.keepRunning){
            if(scanner.hasNextLine()){
//                lines.add(scanner.nextLine());
                System.out.println(scanner.nextLine());
            }
        }
    }

//    public ArrayList<String> getLines(){
//        return lines;
//    }

    public void kill(){
        this.keepRunning = false;
        this.scanner.close();
    }
}